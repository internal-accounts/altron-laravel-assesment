<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $links = \App\Link::all();

    return view('welcome', ['links' => $links]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// admin routes
Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
});

// create posts
Route::get('/author/post', 'HomeController@getPostForm')->name('post.form'); // return the form used to create posts
Route::post('/author/post','HomeController@createPost')->name('post.form'); // save post to DS

// view a list of posts
Route::get('/author/post/detail/{id}', 'HomeController@getPost')->name('post.detail'); //get post by ID

//edit posts
Route::get('/author/post/edit/{id}', 'HomeController@editPost')->name('post.edit');
Route::post('/author/post/edit/{id}', 'HomeController@updatePost')->name('post.update');

// delete post
Route::get('/author/post/delete/{id}', 'HomeController@deletePost')->name('post.delete');


/*
|   Custom Routes
*/

Route::get('/create-new', function(){

    return view('create-new');

});

Route::post('/create-new', function (Request $request) {
    $data = $request->validate([
        'title' => 'required|max:255',
        'url' => 'required|url|max:255',
        'description' => 'required|max:255',
    ]);

    // use the tap() helper function to create a new Link model instance and then save it.
    $link = tap(new App\Link($data))->save();

    return redirect('/');
});

Route::get('/posts', function(){
    return view('post.index');
});
