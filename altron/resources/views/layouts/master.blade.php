<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ URL::to('css/app_foundation.css') }}">
    @yield('styles')
</head>
<body>
@include('partials.header')
@yield('content')

<script type="text/javascript" src="{{ URL::to('js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
