 @if($posts)
                            @foreach($posts as $post)
                                <a class="nav-link" href="{{ route('post.detail', ['id' => $post->id]) }}">{{ $post->title }}</a>
                            @endforeach
                        @else
                            <p class="text-center text-primary">Sorry, no posts created yet!</p>
                        @endif
