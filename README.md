# README

## Altron Laravel Assesment

### Startup
* Clone the Repo locally
* Make sure that you have Composer installed
* Initialize the migration to create db table using the command `php artisan migrate`
* Starting the project, please navigate into the `app` directory and run the following command:
`php artisan serve`

### Database Credentials - Remote Datasource, can be accessed using Workbench
* Database: prologiq_altrondev
* Username: prologiq_blackdev
* Password: @PrologiQ2906
* Host: 164.160.91.12


### Development Process
1. Add The Ability For Users To Login & Register
2. Add The Ability For The User To Create, Update, Delete & Edit Posts
3. Authentication Point: User That Created The Posts can only view it
4. Add User Permissions
